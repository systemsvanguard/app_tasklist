# Numeris Single Source Task List

Numeris Single Source Task List management application.  This is the front-end, built using Angular 12, HTML, CSS, and Bootstrap v 5 CSS framework.  It consumes and gets data from a RESTful API backend using a RDBMS.  It allows us to display, add, search, and manage Single Source Tasks.

## Numeris Task List Application :
- Each Task has id, title, tasktype, weight, description, published status.
- We can create, retrieve, update, delete Tasks.
- There is a Search bar for finding Tasks by title.

![Numeris Single Source Task List ~ Info](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen04.png)


## Steps to Install
- Run the command below from the command line / terminal / command prompt.
- git clone https://systemsvanguard@bitbucket.org/systemsvanguard/app_tasklist.git
- cd app_tasklist\ 
- ensure your have Node & NPM pre-installed. Run commands 'node --version && npm -v'.
- npm install.  (This ensures all dependencies are installed).
- Ensure to rename local ".env4Display" to ".env".  
- Start the server with "ng serve --port 8081" 
- Runs on port 8080 --> http://localhost:8081/. The app technically runs on port 4200 BUT the API exposes its data for consumption by the app, on port 8081.


## Features
- Angular 12 framework
- Bootstrap v5 CSS framework
- FontAwesome 5 icons
- Google Fonts
- Filler text from https://pirateipsum.me
- dotenv & environment variables


## License
This project is licensed under the terms of the **MIT** license.


## Screenshots
![Screen 01](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen01.png)
![Screen 02](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen02.png)
![Screen 03](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen03.png)


