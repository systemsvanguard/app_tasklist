export class Tutorial {
  id?: any;
  title?: string;
  tasktype?: string;
  weighing?: number;
  description?: string;
  published?: boolean;
}
